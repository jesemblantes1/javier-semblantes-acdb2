import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Scanner;
public class GeneradorDePalabrasDeNumerosTelefonicos {
      public static void main(String[] args) throws FileNotFoundException {
           // Iniciando el escáner para leer la entrada del usuario
            Scanner scanner = new Scanner(System.in);
            // obtener el nombre del archivo de salida
            System.out.print("Ingrese el nombre del archivo con su extension .txt (ejemplo: ejem.txt): ");
            String NombreDelArchivo = scanner.nextLine();
            // creando el objeto PrintStream
            PrintStream Stream = new PrintStream(new File(NombreDelArchivo));
            // Obteniendo una palabra de entrada de 7 dígitos como String
            System.out.print("Ingrese los 7 digitos: ");
            String Telefono = scanner.nextLine();
            // asegurando que la longitud es 7 y no contiene 1 o 0
// suponiendo el número de teléfono de otra manera, es válido
            if (Telefono.length() != 7 || Telefono.contains("1") || Telefono.contains("0")) {

                  System.out.println("SOLO NUMEROS TELEFONICOS CON 7 DIGITOS"
                             + " Y NO SE ADMITEN CEROS Y UNOS");
            } else {
                //generando palabras, guardando en un archivo
                  int numPosiblesCombinaciones = EnumerarPalabras(Telefono, Stream);
                  Stream.close();
                  // Mostrando un mensaje
                  System.out.println(numPosiblesCombinaciones
                             + " las combinaciones se escribieron en el archivo " + NombreDelArchivo);
            }
      }
      static char[] getClave(int n) {
            switch (n) {
            case 2:
                  return new char[] { 'A', 'B', 'C' };
            case 3:
                  return new char[] { 'D', 'E', 'F' };
            case 4:
                  return new char[] { 'G', 'H', 'I' };
            case 5:
                  return new char[] { 'J', 'K', 'L' };
            case 6:
                  return new char[] { 'M', 'N', 'O' };
            case 7:
                  return new char[] { 'P', 'Q', 'R', 'S' };
            case 8:
                  return new char[] { 'T', 'U', 'V' };
            case 9:
                  return new char[] { 'W', 'X', 'Y', 'Z' };
            }
            return null;
      }
      static int EnumerarPalabras(String num, PrintStream out) {
            int numCombinaciones = enumerarPalabras(num, "", out);
            out.flush(); 
            return numCombinaciones;
             //llamando al método recursivo para realizar la enumeración

      }
      static int enumerarPalabras(String num, String text, PrintStream out) {
            if (num.length() == 0) {
                // caso base, mostrando el texto
                  out.println(text);
                  // devolviendo 1 como número posible de combinaciones
                  return 1;
            } else {
                // encontrar el dígito en la posición 0
                  int digit = num.charAt(0) - '0';
                  // encontrar posibles teclas de teléfono para este dígito
                  char[] letras = getClave(digit);
                  // Iniciando el conteo de combinaciones
                  int numCombinaciones = 0;
                  if (letras != null) {
                      // haciendo un bucle a través de todas las teclas posibles
                       for (int i = 0; i < letras.length; i++) {
                             numCombinaciones += enumerarPalabras(num.substring(1), text
                                         + letras[i], out);
                       }
                  }
                   // devolviendo el número de combinaciones
                  return numCombinaciones;
            }
      }
}
